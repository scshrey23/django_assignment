# Django_Assignment

This Django Project contains 2 Tables which are inter related by foreign key. 2 Views have been defined for Get, post and put method on the 2 tables separately.  Using Get method, all the data of the table or a particular data containing the same key value pair as passed in url are returned. The user_name is not displayed for person View as it should not be displayed directly due to security reasons. Using Post method, data in table using form-data or urlencoded data or data in JSON format are inserted. Using PUT method, values in the table using urlencoded input data are updated. The filter and exclude function are in comments as they donot have any particular use as per the project. They can be used by uncommenting them. Error Messages and Confirmation Messages are returned using HttpResponse.

**Person Module**
----
  Returns data about a single entry, multiple/all entries, Enters data in table, Update data in Person Table

* **URL**

  /restapp/person/  - For updating data, entering data, getting all the data  
  /restapp/person/?user_name=user  - For getting data of a single entry of key user.

* **Method:**

  `GET, POST, PUT`
  
*  **URL Params**

   **Required:**
 
   `user=[string]` - For GET method for single Entry

* **Data Params**

   For POST and PUT method

  `first_name = [string]`  
  `last_name = [string]`  
  `user_name = [string]`  
  `branch_name = [string]`  

* **Success Response:**

   **Code:** 200  -  For GET & PUT

    OR

   **Code:** 201  -  For POST 
 
* **Error Response:**

   **Code:** 404 NOT FOUND  
    **Content:** `{ error : "Data/Object doesn't exist" }`



**Branch Module**
----
  Returns data about a single entry, multiple/all entries, Enters data in table, Update data in Branch Table

* **URL**

  /restapp/branch/  -  For updating data, entering data, getting all the data  
  /restapp/branch/?branch_name=branch  -  For getting data of a single entry of key user.

* **Method:**

  `GET, POST, PUT`
  
*  **URL Params**

   **Required:**
 
   `branch=[string]` -  For GET method for single Entry

* **Data Params**

   For POST and PUT method

   `branch_name = [string]`  
   `branch_hod = [string]`  
   `branch_hq = [string]`  

* **Success Response:**

   **Code:** 200  -  For GET & PUT 

    OR

   **Code:** 201  -  For POST   
 
* **Error Response:**

   **Code:** 404 NOT FOUND  
    **Content:** `{ error : "Data/Object doesn't exist" }`