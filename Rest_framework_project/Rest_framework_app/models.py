from __future__ import unicode_literals
from django.db import models

class Branch(models.Model):
    branch_name = models.CharField(max_length=255, unique=True)
    branch_hod = models.CharField(max_length=255)
    branch_hq = models.CharField(max_length=255, default = "Bangalore")

    def __unicode__(self):
        return str(self.branch_name)

class Person(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    user_name = models.CharField(max_length=255, unique=True)
    branch_name = models.ForeignKey(Branch, on_delete = models.CASCADE)
    
    def __unicode__(self):
        return str(self.first_name) + ' ' + str(self.last_name) + '  ' + str(self.branch_name) + '\n'