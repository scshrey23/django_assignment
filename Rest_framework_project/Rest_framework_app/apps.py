from django.apps import AppConfig


class RestFrameworkAppConfig(AppConfig):
    name = 'Rest_framework_app'
