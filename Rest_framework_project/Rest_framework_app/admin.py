from django.contrib import admin
from Rest_framework_app.models import Person, Branch

admin.site.register(Person)
admin.site.register(Branch)