from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from Rest_framework_app.views import PersonView, BranchView

urlpatterns = [
    url(r'^person/', csrf_exempt(PersonView.as_view())),
    url(r'^branch/', csrf_exempt(BranchView.as_view())),
]