from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, QueryDict
import json
from django.core.exceptions import ObjectDoesNotExist
from Rest_framework_app.models import Branch, Person

class PersonView(View):

    def get(self, request, *args, **kwargs):    # Returns all the list or a particular value of a key in Person Table

        _arg=request.GET
        
        if _arg:
            try:
                person_object=Person.objects.get(user_name=_arg.get('user_name'))
                # person_object=Person.objects.filter(user_name=_params.get('user_name'))
                # person_object=Person.objects.exclude(user_name=_params.get('user_name'))
            except ObjectDoesNotExist:
                return HttpResponse("Person Do Not Exist!")
        
        else:
            person_object=Person.objects.all()

        return HttpResponse(person_object, content_type='application/json') # User_name is hidden. It is not displayed.

    def post(self, request, *args, **kwargs):   # Insertion of data in Person Model
        
        _arg=request.body
        try:
            _arg=json.loads(_arg)
        except ValueError:
            _arg=request.POST

        first_name=_arg.get('first_name')
        last_name=_arg.get('last_name')
        user_name=_arg.get('user_name')
        branch_name=_arg.get('branch_name')

        try:
            branch_object=Branch.objects.get(branch_name=branch_name)
        except ObjectDoesNotExist:
            return HttpResponse("Foreign Key Error!")

        try:
            person_get=Person.objects.get(user_name = user_name)
        except ObjectDoesNotExist:
            person_object=Person(first_name=first_name,last_name=last_name,user_name=user_name,branch_name=branch_object)
            person_object.save()
            return HttpResponse("Data Inserted Successfully")
        
        return HttpResponse("User Name Already Exist!")

    def put(self, request, *args, **kwargs):  # Updation of Values in Person Model

        _arg=QueryDict(request.body)
        first_name=_arg.get('first_name')
        last_name=_arg.get('last_name')
        user_name=_arg.get('user_name')
        branch_name=_arg.get('branch_name')

        try:
            branch_object=Branch.objects.get(branch_name=branch_name)
        except ObjectDoesNotExist:
            return HttpResponse("Foreign Key Error!")

        person_object=Person.objects.get(user_name=user_name)
        person_object.first_name=first_name
        person_object.last_name=last_name
        person_object.branch_name=branch_object;
        person_object.save();
        return HttpResponse("Table Values Updated!")

class BranchView(View):

    def get(self, request, *args, **kwargs):    # Returns all the list or a particular value of a key in Branch Table

        _params=request.GET
        branch_object=[]

        if _params:
            try:
                branch_object1=Branch.objects.get(branch_name=_params.get('branch_name'))
                branch_object.append({
                    'Branch_name': branch_object1.branch_name,
                    'Branch_hod': branch_object1.branch_hod,
                    'branch_hq': branch_object1.branch_hq,
                    })
            except ObjectDoesNotExist:
                return HttpResponse("Person Do Not Exist!")
        else:
            branch_object1=Branch.objects.all()
            for obj in branch_object1:
                branch_object.append({
                    'Branch_name': obj.branch_name,
                    'Branch_hod': obj.branch_hod,
                    'branch_hq': obj.branch_hq,
            })
        return HttpResponse(json.dumps(branch_object), content_type='application/json')

    def post(self, request, *args, **kwargs):   # Insertion of data in Branch Model
        
        _arg=request.body
        try:
            _arg=json.loads(_arg)
        except ValueError:
            _arg=request.POST

        branch_name=_arg.get('branch_name')
        branch_hod=_arg.get('branch_hod')
        branch_hq=_arg.get('branch_hq')
        

        try:
            branch_object=Branch.objects.get(branch_name=branch_name)
        except ObjectDoesNotExist:
            branch_object=Branch(branch_name=branch_name,branch_hod=branch_hod,branch_hq=branch_hq)
            branch_object.save()
            return HttpResponse("Data Inserted Successfully")
        
        return HttpResponse("Branch Name Already Exist!")

    def put(self, request, *args, **kwargs):  # Updation of Values in Branch Model

        _arg = QueryDict(request.body)
        branch_name=_arg.get('branch_name')
        branch_hod=_arg.get('branch_hod')
        branch_hq=_arg.get('branch_hq')

        try:
            branch_object=Branch.objects.get(branch_name=branch_name)
        except ObjectDoesNotExist:
            return HttpResponse("Branch Name Does Not Exist")

        branch_object.branch_hod=branch_hod
        branch_object.branch_hq=branch_hq
        branch_object.save();
        return HttpResponse("Table Values Updated!")